@extends('_layout.layout')

@section('head_title', __('Uredništvo') . ' - ' . __('Filemanager'))

@php
    $timestamp = date('d-H');
@endphp

@push('head_links')
<link href="{{ asset('custom/js/plupload-3.1.2/js/jquery.plupload.queue/css/jquery.plupload.queue.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset('/theme/plugins/sweet-alert2/sweetalert2.min.css')}}?v={{$timestamp}}" rel="stylesheet" type="text/css">
@endpush

@section('content')
<style>
    div.single-file a.file-img-buttons{
        font-size: 22px;
        color: white;
        border: 1px solid #4a4a4a;
        background-color: rgb(248, 148, 6);
        color: white;
        border: 1px solid white;
        border-radius: 4px;
        width: 40px;
        text-align: center;
        display: inline-block;
    }

    .single-file{
        position: relative;
    }

    .file-buttons{
        position: absolute;
        display: none;
        margin-bottom: 10px;
        bottom: 0px;
        right: 7px;
        font-size: 0;
    }

    div.single-file a.file-img-buttons:hover{
        font-size: 36px;
        color: white;
    }
    .single-file:hover .file-buttons{
        display: block;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-heading d-flex justify-content-between align-items-center">
                <div class="card-heading-title">
                    <h3 class="card-title">@lang('Unos fajlova')</h3>
                </div>
            </div>
            <div class="card-body pt-1">
                <ul class="nav nav-tabs mb-2" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="preview-tab-1" data-toggle="tab" href="#tekst-1" role="tab"
                           aria-controls="tekst-1"
                           aria-selected="false">
                            <span class="d-block d-sm-none"><i class="fa fa-home"></i></span>
                            <span class="d-none d-sm-block">@lang('Preview')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="upload-tab-1" data-toggle="tab" href="#slike-1" role="tab"
                           aria-controls="slike-1" aria-selected="true">
                            <span class="d-block d-sm-none"><i class="fa fa-user"></i></span>
                            <span class="d-none d-sm-block">@lang('Upload')</span>
                        </a>
                    </li>
                </ul>
                <div class="row">
                    <div id="files-uploaded" class="col-8">
                        @foreach($files as $file)
                            @include('filemanager::partials.file')
                        @endforeach
                    </div>
                    <div class="col-4 filemanager-filter">
                        <div class="inputs-wrapper">
                            <div class="card">
                                <div class="card-heading d-flex justify-content-between">
                                    <div class="card-heading-title">
                                        <h3 class="card-title">Filteri</h3> 
                                            <p class="card-sub-title text-muted">Filteri fotografija</p>
                                    </div> 
                                    <div class="card-heading-actions"></div>
                                </div>
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-4 control-label">Ime fajla</label>
                                        <div class="col-sm-8">
                                            <span>
                                                <div class="ui-widget">
                                                    <input name="search-file" class="form-control ui-autocomplete-input" placeholder="Unesite termin za pretragu" autocomplete="off">
                                                </div> 
                                                <span class="invalid-feedback" style="display: none;"></span>
                                            </span>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group row pt-4">
                            <div class="col-sm-6">
                                {{-- <button type="button" class="btn btn-warning waves-effect waves-light">
                                    <i class="fa fa-refresh"></i>
                                        Resetuj
                                </button> --}}
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="button" class="btn btn-secondary waves-effect waves-light search-files">
                                    <i class="fa fa-search"></i>
                                            @lang('Pretraži')
                                </button>
                            </div> 
                            
                        </div>
                    </div>
                </div>
                <div id="files-uploader" class="d-none">
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer_scripts')
<script src="{{ asset('custom/js/plupload-3.1.2/js/plupload.full.min.js') }}"></script>
<script src="{{ asset('custom/js/plupload-3.1.2/js/jquery.plupload.queue/jquery.plupload.queue.min.js') }}"></script>
<script src="{{ asset('custom/js/plupload-3.1.2/js/i18n/' . app()->getLocale() . '.js') }}"></script>
<script src="{{ asset('js/app.js' )}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{asset('/theme/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>

<script type="text/javascript">

    function searchFile(){
        let searchTerm = $('.filemanager-filter [name="search-file"]').val();

            $.ajax({
                "url": "{{ route('filemanager.search') }}",
                "type": "get",
                "data": {
                    'searchTerm': searchTerm
                }
            }).done(function(response){
                $('#files-uploaded').html(response.data.files_view);
            });
    }
    
    $(function() {
        var uploader = $('#files-uploader').pluploadQueue({
            // General settings
            runtimes : 'html5,flash,silverlight,html4',
            url : "{{route('filemanager.upload')}}",
            
            chunk_size : '1mb',
            dragdrop: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept': 'application/json' 					
            },
            file_data_name: 'my_file',
            multiple_queues: true,
            
            filters : {
                // Maximum file size
                max_file_size : '15mb',
                // Specify what files to browse for
                mime_types: [
                    {title : "Image files", extensions : "jpg,jpeg,png,webp,svg,png"},
                    {title : "Zip files", extensions : "zip,rar"},
                    {title : "Txt files", extensions : "doc,docx"},
                    {title : "Pdf files", extensions : "pdf"},
                    {title : "Other files", extensions : "js,css,html"}
                ]
            },
            init: {
                'FilesAdded': function(up, files) { 
                    up.start();
                },
                'BeforeUpload': function(up, file) { 
                    if (up['upload_with_errors'] < 0) {
                        up['upload_with_errors'] = 0;
                    }
                },
                'FileUploaded': function(up, file, info) {
                    var response;
                    try {
                        response = $.parseJSON(info.response);
                    } catch (e) {
                        file.status = plupload.FAILED;	
                        up['upload_with_errors'] ++;	
                        return;
                    }
                    $('#files-uploaded').append(response.result.html);	
                },
                'UploadComplete': function(up, files) { 
                    if (up['upload_with_errors'] > 0) {
                        showSystemMessage('Došlo je do greške prilikom otpremanja vašeg fajla', 'error');
                    }
                    
                    up['upload_with_errors'] = -1;
                },
                'Error': function(up, args) { 
                    if (args['code'] && args['code'] == plupload.HTTP_ERROR) {
                        showSystemMessage('Došlo je do greške prilikom otpremanja vašeg fajla', 'error');
                    } else if (args['message']) {
                        showSystemMessage(args['message'], 'error');
                    }
                } 					
            }
        });

        $('#files-uploaded').questionPop({
            "liveSelector": '[data-action="delete"]'
        }).on('success.qp', function(response) {
            $('#files-uploaded .single-file[data-id="' + response.target.dataset.id + '"]').remove();
        });

        $('#upload-tab-1').on('click', function(){
            $('div#files-uploaded').empty();
            $('div#files-uploader').removeClass('d-none');
            $('.filemanager-filter').addClass('d-none');
        });

        //na klik preview taba dohvati preko ajax rute fajlove
        $('#preview-tab-1').on('click', function(){
            $('div#files-uploader').addClass('d-none');
            $('.filemanager-filter').removeClass('d-none');
            $.ajax({
                "url": "{{ route('filemanager.all') }}",
                "type": "get"
            }).done(function(response){
                $('#files-uploaded').html(response.data.files_view);
            });
        });

        //na klik preview taba dohvati preko ajax rute fajlove
        $('.search-files').on('click', function(){
            searchFile();
        });

        $('.filemanager-filter [name="search-file"]').keypress(function (e) {
            var key = e.which;
            if(key == 13){
                searchFile();
                return false;  
            }
        }); 
    });
    
</script>
@endpush