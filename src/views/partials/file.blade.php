
<div class="single-file" style="display:inline-block; border-style:double; border-color:#313a46; width: 160px" data-id="{{ $file['id'] }}">
    <p class="files" style="background: #313a46; color:white; margin-bottom: 0px; padding-left:3px; padding-right:3px;" title="{{$file['filename']}}">
        {{\Str::limit($file['filename'], 16, '...')}}
    </p>
    <img src="filemanager/img/file-img.jpeg" alt="file" width="100" height=120>
    <div class="file-buttons">
        <a class="file-img-buttons" href="{{$file['filepath']}}" download="">
            <i class="fa fa-download"></i>
        </a>
        <a class="file-img-buttons" 
            {{-- href="{{$file['filepath']}}" --}}
            title="@lang('Izbriši')"
            data-action="delete" 
            data-title="@lang('Izbriši')"
            data-id="{{ $file['id'] }}"
            data-text="@lang('Da li ste sigurni da želite da obrišete fajl #~b~#')"
            data-label="{{ $file['filename'] }}"
            data-ajax-url="{{ route('filemanager.delete', $file['id']) }}"
        >
            <i class="fa fa-trash"></i>
        </a>
    </div>
</div>

