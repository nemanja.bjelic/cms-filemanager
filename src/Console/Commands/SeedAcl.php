<?php

namespace Cubes\Filemanager\Console\Commands;

use Illuminate\Console\Command;

class SeedAcl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:acl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeder za acl';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //provera postojanja tabela
        if(!\Schema::hasTable('acl_permissions')){
            $this->error('There is no acl_permissions table.');
            return;
        }

        if(!\Schema::hasTable('acl_group_has_permissions')){
            $this->error('There is no acl_group_has_permissions table.');
            return;
        }
        //upis u tabelu filemanager permisije i uzimanje id-ja novog reda
        $filemanagerId = \DB::table('acl_permissions')->insertGetId(
            ['name' => 'filemanager',
             'slug' => 'filemanager',
             'created_at' => now(),
             'updated_at' => now(),
            ]
        );

        //dodavanje id-ja iz tabele acl_permission u acl_group_has_permission
        foreach(config('filemanager.acl_group_ids') as $groupId){
            \DB::table('acl_group_has_permissions')->insert([
                'group_id' => $groupId,
                'permission_id' => $filemanagerId,
            ]);
        }
        
        $this->info('New permission successfuly added!');
    }
}