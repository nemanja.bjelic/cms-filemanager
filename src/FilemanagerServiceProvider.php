<?php

namespace Cubes\Filemanager;

use Illuminate\Support\ServiceProvider;
use Cubes\Filemanager\Console\Commands\SeedAcl;

class FilemanagerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/views', 'filemanager');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->publishes([
            __DIR__.'/public' => public_path('filemanager'),
        ], 'filemanager');
        $this->publishes([
            __DIR__.'/config/filemanager.php' => config_path('filemanager.php'),
        ], 'filemanager');

        if ($this->app->runningInConsole()) {
            $this->commands([
                SeedAcl::class,
            ]);
        }
    }

    public function register()
    {

    }
}