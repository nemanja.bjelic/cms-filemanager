<?php

Route::group(['namespace' => 'Cubes\Filemanager\Http\Controllers'], function(){
    Route::get('filemanager', 'FilemanagerController@index')
    ->middleware(['web', 'auth'])
    ->middleware('can:filemanager')->name('filemanager.index');
    Route::post('filemanager', 'FilemanagerController@upload')->name('filemanager.upload');
    Route::post('{file}/delete', 'FilemanagerController@delete')->name('filemanager.delete');
    Route::get('/ajax-all', 'FilemanagerController@all')->name('filemanager.all');
    Route::get('/ajax-search', 'FilemanagerController@search')->name('filemanager.search');
});
