<?php
return [
    //define table name
    'table_name' => 'files',

    //define array of group ids for permissions
    'acl_group_ids' => [1]
];