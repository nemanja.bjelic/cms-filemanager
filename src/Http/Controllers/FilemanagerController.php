<?php

namespace Cubes\Filemanager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JildertMiedema\LaravelPlupload\Facades\Plupload;
use Cubes\Filemanager\Models\File;
use App\Http\Resources\JsonResource;

class FilemanagerController extends Controller
{

    public function index()
    {
        $files = File::latest()->limit(20)->get();
        
        return view('filemanager::filemanager', [
            'files' => $files,
        ]);
    }

    public function upload(Request $request)
    {
        $data = $request->validate([
            'my_file' => ['required', 'file', 'mimes:doc,docx,jpg,jpeg,png,webp,svg,png,zip,rar,pdf,js,css,html'],
        ]);

        return Plupload::receive('my_file', function ($file)
        {
            $fileEntity = new File();
            $fileEntity->filename = $file->getClientOriginalName();
            $fileEntity->save();
            $fileName = '';

            if($file->storeAs('files', $fileEntity->id . '-' . $file->getClientOriginalName(), 'public')){
                $fileEntity->filepath = '/data/files/' . $fileEntity->id . '-' . $file->getClientOriginalName();
                $fileEntity->filename = $fileEntity->id . '-' . $file->getClientOriginalName();
                $fileName = $fileEntity->filename;
                $fileEntity->save();
            }else{
                $fileEntity->delete();
            }
            
            return [
                'file' => $fileEntity->toArray(),
                'html' => view('filemanager::partials.file', ['file' => $fileEntity->toArray(), 'realName' => $fileName])->render()
            ];
        });
        
    }

    public function delete($file, Request $request)
    {
        
        $file = File::find($file);
        // dd($file);
        $file->delete();
        $message = __('Fajl je uspešno obrisan!');

        if ($request->wantsJson()) {
            return JsonResource::make(['id' => $file['id']])->withSuccess($message);
        }
        
        return redirect()->back()->withSystemSuccess($message);
    }

    public function all()
    {
        $files = File::latest()->limit(20)->get();
        
        return JsonResource::make([
            'files_view' => view('filemanager::partials.files', [
            'files' => $files
        ])->render()]);
    }

    public function search()
    {
        $data = request()->validate([
            'searchTerm' => ['nullable', 'string', 'max:191']
        ]);

        if(empty($data['searchTerm'])){
            $files = File::latest()->limit(20)->get();
        }else{
            $searchTerm = $data['searchTerm'];

            $files = File::where('filename', 'like', '%' . $searchTerm . '%')->get();
        }
        return JsonResource::make([
            'files_view' => view('filemanager::partials.files', [
            'files' => $files
        ])->render()]);
    }
}