<?php

namespace Cubes\Filemanager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use softDeletes;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'filename',
        'filepath',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->setTable(config('filemanager.table_name'));
    }
}
