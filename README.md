# Filemanager paket

## Ovaj paket dozvoljava samo korisnicima sa permisijom da upload-uju fajlove na sistem

Publish-ovati konfig fajl i podesiti ime baze, kao i uneti niz "acl_group_ids" za koje grupe zelimo permisije.
Takodje dobijamo u public folderu i ikonicu za fajlove pa je mozemo zameniti.
 - php artisan vendor:publish --provider="Cubes\Filemanager\FilemanagerServiceProvider" --tag=filemanager

### Migracija
Prvo pokrenuti migraciju koja kreira tabelu sa nazivom iz filemanager konfig fajla
### Seeder za acl permisije
Seeder se pokrece pomocu konzolne komande:
 - php artisan seed:acl

### Rute
Ime rute je 'filemanager.index' ili ukoliko ne radi iz nekog razloga name putanja je '/filemanager'